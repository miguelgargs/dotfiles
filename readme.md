# :wrench: .files

_This is a **snapshot** my ([**@bookercodes**](https://twitter.com/bookercodes))
dot files. Whilst there's an
[`install.sh`](https://github.com/alexbooker/dotfiles/blob/master/install.sh) script,
it's really intended for my own use. You probably won't have much success with
it. You'll probably be better off reading the individual dot files and copying
the bits that interest you._
